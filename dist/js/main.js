
 $(function() {


    // Aside click

    $(document).on('click', '.question__item', function(e) {
        e.stopPropagation();
        $('.question__content').removeClass('show');
        $('.question__cross').removeClass('show');
        $(e.target).find('.question__cross').addClass('show');
        $(e.target).find('.question__content').addClass('show');
    });

    $(document).on('click', '.question__cross', function(e) {
        $(e.target).parent().find('.question__cross').removeClass('show');
        $(e.target).removeClass('show');
    });


    $('aside').on('click',function(e) {

        if (e.target == this) {
            $('.question__content').removeClass('show');
            $('.question__cross').removeClass('show');
        }
    });




    // Block click

    $(document).on('click', '.block[data-content]', function(e) {
        var that = $(e.target),
            close = that.attr('data-close'),
            id = that.attr('data-content');

        that.siblings().addClass('disable');
        that.removeClass('disable');

        $(close).hide();
        $(id).show();
    });
 });

